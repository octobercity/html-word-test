var HtmlDocx = require('html-docx-js');
var fs = require('fs');

/**官方通过命令输人参数  命令行输入  node index.js html2word.html  output.docx*/
// var inputFile = process.argv[2]; //fs.createReadStream("./html2word.html");;
// var outputFile = process.argv[3];


//直接输出语句
fs.readFile("./html2word.html", 'utf-8', function(err, html) {
  if (err) throw err;// 2.0 修改//做特殊处理
//使用buffer，如果使用stream要转换
  var docx = HtmlDocx.asBlob(html);
  fs.writeFile("./test.docx", docx, function(err) {
    if (err) throw err;
    console.log("==============");//附带2.0 update
  });
});
